# Instrucciones

Ejecutar para la instalación de Jest
```bash
npm i
```

Una vez instalado jest ejecutar
```bash
tsc index.ts
```
Una vez que se cree el archivo index.js ejecutar
```bash
node index.js
```
Para que la salida se pueda ver en consola.

Para ejecutar las pruebas ejecutamos el siguiente comando
```bash
npm run test
```