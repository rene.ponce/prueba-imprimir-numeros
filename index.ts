const imprimeNumeros = () => {
  const reemplazar = ['Music', 'TI', 'Musical'];
  let contenedor = [];
  for (let i = 1; i <= 100; i++) {
    ((i % 3 == 0 || i % 5 == 0) 
       && contenedor.push(reemplazar[Number(i % 3 == 0 && i % 5 >= 1) + (Number(i % 3 == 0 && i % 5 == 0) * 2)])) 
       || contenedor.push(i);
  }
  console.log(contenedor);
  return contenedor;
};

imprimeNumeros();

const functions = {
  imprimeNumeros
}

module.exports = functions;