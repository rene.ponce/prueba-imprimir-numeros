const functions = require('./index');

describe('imprimirNumeros', () => {
  const expected = ['Music', 'TI', 'Musical'];
  it('Debe contener Music, IT, Musical', () => {
    expect(functions.imprimeNumeros()).toStrictEqual(expect.arrayContaining(expected));
  });
});